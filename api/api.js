'use strict';

const express = require('express');
const router = express.Router();
 
require('dotenv').config();

router.get('/ping', function (req, res) {
  return res.status(200).json({ success: true, data: 'ping ok' });
});
 
router.get('/*', function (req, res) {
  console.log('unknow call');
});

module.exports = router;

 