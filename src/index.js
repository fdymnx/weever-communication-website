import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import {makeStyles, createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';

import {BrowserRouter} from 'react-router-dom';
import NavHeader from './components/NavHeader';
import Footer from './components/Footer';
import Config from './Config';
import {Switch, Route} from 'react-router-dom';
import Home from './pages/Home';
import Concept from './pages/Concept';
import Support from './pages/Support';
import Contact from './pages/Contact';
import Credits from './pages/Credits';
import Roadmap from './pages/Roadmap';
import MentionsLegales from './pages/MentionsLegales';
import Suivi from './pages/casusages/Suivi';
import Agregation from './pages/casusages/Agregation';
import Editorialisation from './pages/casusages/Editorialisation';
import Documentation from './pages/casusages/Documentation';

import useMediaQueries from './utilities/useResponsive';
import clsx from 'clsx';

const theme = createMuiTheme({
  typography: {
    fontFamily: ['Lato', 'Segoe UI Regular', 'Segoe UI', 'Roboto', '"Helvetica Neue"', 'Arial', 'sans-serif'].join(','),
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1380,
      xl: 1920,
    },
  },
});

const useStyles = makeStyles((theme) => ({
  indexRoot: {
    overflow: 'hidden',
    fontSize: 'calc(10px + 4vmin)',
    height: '100%',
    minHeight: '600px',
    backgroundColor: Config.colors.white,
    color: Config.colors.dark,
    display: 'flex',
    flexDirection: 'column',
  },
  indexDesktop: {
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  indexMobile: {
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'column',
  },
  indexContent: {
    width: '100%',
    maxWidth: '1000px',
    flex: 1,
    margin: 'auto',
    padding: Config.logoTop,
    paddingBottom: '20px',
    [theme.breakpoints.up(Config.theme.breakpoints)]: {
      padding: '25px',
      paddingTop:'0px'
    },
    display: 'flex',
    // flex: '1 0 auto' /* Prevent Chrome, Opera, and Safari from letting these items shrink to smaller than min size. */,
    zIndex: 99
  },
}));

function App() {
  const classes = useStyles();
  const {isDesktop} = useMediaQueries();

  return (
    <MuiThemeProvider theme={theme}>
      <div className={classes.indexRoot}>
        <div className={clsx(isDesktop ? classes.indexDesktop : classes.indexMobile)}>
          <NavHeader isDesktop={isDesktop} />

          <div className={classes.indexContent}>
            <Switch>
              <Route path="/concept" component={Concept} />
              <Route path="/support" component={Support} />
              <Route path="/contact" component={Contact} />

              <Route path="/suivi" component={Suivi} />
              <Route path="/agregation" component={Agregation} />
              <Route path="/editorialisation" component={Editorialisation} />
              <Route path="/documentation" component={Documentation} />

              <Route path="/credits" component={Credits} />
              <Route path="/roadmap" component={Roadmap} />
              <Route path="/mentionslegales" component={MentionsLegales} />

              <Route component={Home} />
            </Switch>
          </div>
        </div>
        <Footer />
      </div>
    </MuiThemeProvider>
  );
}

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

/*
    if (isDesktop) {
      return (
        <MuiThemeProvider theme={theme}>
          <div className={clsx(classes.indexRoot, classes.bgImage)}>
            <Grid container direction="row" justify="flex-start" alignItems="stretch">
              <Grid item className={gS.tester}>
                <NavHeader isDesktop={isDesktop} />
              </Grid>
              <Grid item xs={12} sm className={clsx(gS.tester2)}>
                <div className={classes.indexContent}>
                  <Switch>
                    <Route component={Home} />
                  </Switch>
                </div>
              </Grid>
            </Grid>

            <Footer />
          </div>
        </MuiThemeProvider>
      );
    }*/
