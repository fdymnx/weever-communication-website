const Config = {
  colors: {
    dark: '#000000',
    white: 'white',
    grey: '#E6E6E6',
    green :'#A6D865',
    blue: '#1C97C3',
  },
  footer: {
    logoWidth: 350,
  },
  navbar: {
    logoSize: 140,
  },
  logoTop: 20,
  theme: { breakpoints: 'sm' }, 
};
export default Config;
