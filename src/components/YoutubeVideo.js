import React from 'react';
import PropTypes from 'prop-types';

YoutubeVideo.propTypes = {
  src: PropTypes.string.isRequired,
};

function YoutubeVideo({ src }) {
  return (
    <iframe
      title="video"
      width="560"
      height="315"
      src={src}
      frameBorder="0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen
      style={{
        left: '0',
        top: '0',
        height: '100%',
        width: '100%',
        position: 'absolute',
      }}
    ></iframe>
  );
}

export default YoutubeVideo;
