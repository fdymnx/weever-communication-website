import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { IconButton } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';

import clsx from 'clsx';
import Config from '../../Config';
import ImportedGraphics from '../../ImportedGraphics';

const useStyles = makeStyles((theme) => ({
  logoContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: 10,
  },
  logo: {
    height: `${Config.navbar.logoSize / 5}px`,
    width: `${Config.navbar.logoSize / 5}px`,
    margin: 'auto',
  },
  root: {
   flex:1, 
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: Config.colors.grey,
  },
}));

/**
 * Contenu de menu de navigation
 * @param {function} onOpen callback pour ouvrir la modal
 */
export function NavMobile({ onOpen }) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <a className={classes.logoContainer} href="/">
        <img alt="Weever" src={ImportedGraphics.logo} className={classes.logo} />
      </a>

      <IconButton aria-label="close" onClick={onOpen} color="inherit">
        <MenuIcon className={classes.menuIcon} />
      </IconButton>
    </div>
  );
}

// bandeau pour fermer la modal
export function NavClose({ onClose }) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={clsx(classes.logoContainer, classes.logo)} />

      <IconButton aria-label="open" onClick={onClose} color="inherit">
        <CloseIcon className={classes.menuIcon} />
      </IconButton>
    </div>
  );
}
