import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {useLocation} from 'react-router-dom';
import clsx from 'clsx';
import Config from '../../Config';
import globalStyles from '../../globalStyles';
import ImportedGraphics from '../../ImportedGraphics';
import UseCase from './UseCase';

const useStyles = makeStyles((theme) => ({
  NavContentRoot: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'column',
    backgroundColor: Config.colors.grey,
    overflowY: 'auto',
    minWidth: '310px',
  },
  logoContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: Config.logoTop / 2,
  },
  logo: {
    height: `${Config.navbar.logoSize}px`,
    width: `${Config.navbar.logoSize}px`,
    margin: 'auto',
  },
  paddingTop: {
    paddingTop: Config.logoTop,
  },
  closeContainer: {
    display: 'flex',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  bottom: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    width: '80%',
  },
  greyTxt: {
    color: 'grey',
    '&:hover': {
      color: Config.colors.blue,
    },
  },
}));

/**
 * Contenu de menu de navigation, affiché en mobile (dans une modal géré dans parent) ou desktop (à gauche)
 */
export default function NavContent({isDesktop}) {
  const classes = useStyles();
  const gS = globalStyles();
  let location = useLocation();

  const entries = [
    {
      href: "/concept",
      title: "Qu'est-ce que Weever"
    }, {
      component: <UseCase />
    }, {
      href: "/roadmap",
      title: "Roadmap contributive"
    }, {
      target: {target: "_blank"},
      href: "https://gitlab.com/groups/mnemotix/weever-community/-/issues",
      title: "Support"
    }, {
      href: "mailto:contact@mnemotix.com",
      title: "Contact"
    }
  ];


  function createEntry({href, title, target, component}) {
    if (component) {
      return component;
    }
    return <a href={href} {...target} className={clsx(gS.p20, gS.ahref, gS.latoBlack, gS.font14, location.pathname === href && gS.selected)}> {title}</ a>
  }

  return (
    <div className={clsx(classes.NavContentRoot)}>
      <div>
        <div className={clsx(isDesktop && classes.paddingTop)} />
        <a className={classes.logoContainer} href="/">
          <img alt="Weever" src={ImportedGraphics.logo} className={classes.logo} />
        </a>
        <div>
          {entries.map((item, index) => (
            <div key={index} className={clsx(gS.flexCenter)}>
              {createEntry(item)}
            </div>
          ))}
        </div>
      </div>
      <div className={clsx(gS.p20, classes.bottom)}>
        {/* <a className={clsx(gS.ahref,gS.font12, classes.greyTxt, location.pathname === "/credits" && gS.selected)} href="/credits">
          Crédits
        </a> */}
        <a className={clsx(gS.ahref, gS.font12, classes.greyTxt, location.pathname === "/mentionslegales" && gS.selected)} href="/mentionslegales">
          Mentions légales
        </a>
      </div>
    </div>
  );
}
