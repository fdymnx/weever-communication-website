import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles'; 

import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import clsx from 'clsx';
import globalStyles from '../../globalStyles';
import {menuEntries} from '../../pages/Home/IconEntries';
import { useLocation } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  whiteFrame: {
    backgroundColor: 'white',
    width: '100%',
    margin: '0px 30px 15px 30px',
    padding: '15px 0px',
  },

  root: {
    padding: '20px 20px 0px 20px !important',
  },
}));

/**
 * Cas d'usages
 */
export default function UseCase({}) {
  const classes = useStyles();
  const gS = globalStyles();
  let location = useLocation();

  const [useCaseOpened, setUseCaseOpened] = useState(menuEntries.some((el) => el.href === location.pathname));

  return (
    <div className={clsx(classes.root, gS.flexCenter, gS.flexDirectionCol, gS.width100)}>
      <div
        onClick={() => setUseCaseOpened(!useCaseOpened)}
        className={clsx(gS.flexCenter, gS.flexDirectionCol, gS.ahref, gS.latoBlack, gS.font14, gS.pointerCursor)}
      >
        Cas d'usages
        {useCaseOpened ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
      </div>

      {useCaseOpened && (
        <div className={classes.whiteFrame}>
          {menuEntries.map((item, index) => (
            <div key={index} className={clsx(gS.p10, gS.flexCenter)}>
              <a className={clsx(gS.ahref, gS.latoBlack,gS.font14, location.pathname === item.href && gS.selected)} href={item.href}>
                {item.menuTxt}
              </a>
            </div>
          ))}
        </div>
      )}
    </div>
  );
}
