import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Modal } from '@material-ui/core';
import NavContent from './NavContent';
import { NavClose, NavMobile } from './NavMobile';
import Config from '../../Config';

const useStyles = makeStyles((theme) => ({
  rootDesktop: {
    WebkitBoxShadow: '10px 10px 29px 1px rgba(0,0,0,0.41)',
    MozBoxShadow: '10px 10px 29px 1px rgba(0,0,0,0.41)',
    boxShadow: '10px 10px 29px 1px rgba(0,0,0,0.41)',
    paddingLeft: Config.logoTop,
    paddingRight: Config.logoTop,
    minWidth: '255px',
    minHeight: '600px',
    [theme.breakpoints.down(Config.theme.breakpoints)]: {
      width: '200px',
      height: '600px',
    },
    display: 'flex',
    backgroundColor: Config.colors.grey,
  },
  rootMobile: {
    width: '100%',
   // height: '100%',
  },
  modalMobile: {
    width: '100%',
    height: '100%',
    overflowY: 'auto',
  },
}));

/**
 * header avec la barre de navigation
 * s'adapte au mobile ou desktop avec une mise en place différente
 * en desktop : s'affiche toujours à gauche
 * en mobile : affiche un bandeau avec icon weever et burger menu a droite , au clic ouvre une modale avec le contenu identique au desktop avec tous les liens
 */
export default function NavHeader({ isDesktop }) {
  const classes = useStyles();
  const [openMobile, setOpenMobile] = useState(false);

  const handleClose = () => {
    setOpenMobile(false);
  };

  const handleOpen = () => {
    setOpenMobile(true);
  };

  if (isDesktop) {
    return (
      <div className={classes.rootDesktop}>
        <NavContent isDesktop={true} />
      </div>
    );
  } else {
    return (
      <div className={classes.rootMobile}>
        <NavMobile onOpen={handleOpen} />
        <Modal open={openMobile} onClose={handleClose} className={classes.modalMobile}>
          <>
            <NavClose onClose={handleClose} />
            <NavContent isDesktop={false} />
          </>
        </Modal>
      </div>
    );
  }
}
