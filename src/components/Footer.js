import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Config from '../Config';
import globalStyles from '../globalStyles';
import ImportedGraphics from '../ImportedGraphics';

const useStyles = makeStyles((theme) => ({
  footer: {
    width: '100%',
    flexShrink: 0,
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
  },
  copyright: {
    width: `min(70vw,${Config.footer.logoWidth}px)`,
    height: 'auto',
  },
  noPM : {
    marginBottom: "-8px",
    padding :'0px ! important'
  }
}));

// affiche le footer avec l'icon en svg contenant directement le copyright, responsive et simple
export default function Footer() {
  const classes = useStyles();
  const gS = globalStyles();
  const thisyear = new Date().getFullYear();
  const src = ImportedGraphics['copyright' + thisyear] || ImportedGraphics['copyright'];
  return (
    <footer className={classes.footer}>
      <a className={classes.noPM} href="https://www.mnemotix.com/" target="_blank">
        <img src={src} className={classes.copyright} />
      </a>
    </footer>
  );
}

/*const useStyles = makeStyles((theme) => ({
  footer: {
    width: '100%',
    flexShrink: 0,
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
  },
  copyright: {
    fontSize:  "calc(7px + 2vmin)",
    lineHeight: '24px',
    background: `url(${ImportedGraphics.footer}) no-repeat 100% 0%`,
    backgroundSize: `min(70vw,${Config.footer.logoWidth}px)`,
    width: `min(70vw,${Config.footer.logoWidth}px)`,
    minHeight: '55px',
    display: 'flex',
  },

  footerahref: {
    flex: 1,
    listStyle: 'none',
    textDecoration: 'none',
    color: '#707e8c',
    margin: 'auto',
    paddingLeft:'calc(7px + 2vmin)'
  },
}));

export default function Footer() {
  const classes = useStyles();
  const gS = globalStyles();
  const year = new Date().getFullYear() || 2021;

  return (
    <footer className={classes.footer}>
      <div className={classes.copyright}>
        <a href="/" className={classes.footerahref}>
          © {year} mnemotix
        </a>
      </div>
    </footer>
  );
}
*/
