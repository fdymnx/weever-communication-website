import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import globalStyles from '../globalStyles';
import Config from '../Config';

const useStyles = makeStyles((theme) => ({
  container: {
    width: '100%',
    flex: 1,
    margin: Config.logoTop * 3,
    [theme.breakpoints.down('sm')]: {
      margin: Config.logoTop,
    },
    padding: Config.logoTop,
  },
  title: {
    fontSize: '6rem',
    fontFamily: 'LatoBlack, sans-serif',
    fontWeight: 'bold',
    marginBottom: '20px',
    [theme.breakpoints.down('sm')]: {
      fontSize: 'calc(25px + 2vmin)',
    },
  },
  content: {fontSize: '2.0rem'}
}));

export default function PageContainer({title, children}) {
  const classes = useStyles();
  const gS = globalStyles();


  return (
    <div className={classes.container}>
      <div className={classes.title}>{title}</div>
      <div className={classes.content}>{children }</div>
    </div>
  );
}
