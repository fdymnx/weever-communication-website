import React from 'react';
import PageContainer from '../../components/PageContainer';
import ImportedGraphics from '../../ImportedGraphics';
import globalStyles from '../../globalStyles';

export default function Agregation(props) {
  const gS = globalStyles();

  return (
    <PageContainer title="Agrégation et exploration de ressources documentaires">
      <div>
        <p> <b> Contexte: </b>
          Weever a été adopté par Atelier Luma afin de consolider sa vocation de plateforme de connaissances à travers le prototypage d’un centre de ressources. Weever y est conçu comme un outil d’agrégation et exploration compréhensive et augmentée des différentes ressources de l’atelier (documents d’archives, matériaux, réseaux, livres, individus…) à travers plusieurs types : les archives, les ouvrages de la bibliothèque, les échantillons de matériaux ainsi que les machines / outils techniques.
          </p>
        <img alt="workflow Luma" src={ImportedGraphics.workflowLuma} className={gS.maxWidthImg}/>
      </div>
    </PageContainer>
  );
}
