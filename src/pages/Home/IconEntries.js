import React from 'react';
import {Grid} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import ImportedGraphics from '../../ImportedGraphics';
import Config from '../../Config';
import {UnstyledLink} from "../../components/widgets/UnstyledLink";

const useStyles = makeStyles((theme) => ({
  entrieContainer: {
    // arret ici
    [theme.breakpoints.up(Config.theme.breakpoints)]: {
      height: 'max(12vmax,225px)',
    },
  },
  entrie: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  entrieLogo: {
    height: `min(10vw,150px)`,
    width: 'auto',
    [theme.breakpoints.down(Config.theme.breakpoints)]: {
      height: `max(15vw,75px)`,
      padding: '15px 0px',
    },
  },
  entrieText: {
    fontFamily: 'LatoBlack, sans-serif',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    fontSize: '3rem', // 'calc(10px + 2vmin)',
    paddingLeft: '15px',
    [theme.breakpoints.between(Config.theme.breakpoints, 'lg')]: {
      fontSize: 'calc(16px + 1vmin)',
    },
    [theme.breakpoints.down(Config.theme.breakpoints)]: {
      fontSize: '1.6rem',
    },
  },
  alignTop: {
    [theme.breakpoints.up(Config.theme.breakpoints)]: {
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'flex-start',
      flexDirection: 'column',
    },
  },
  alignBottom: {
    [theme.breakpoints.up(Config.theme.breakpoints)]: {
      display: 'flex',
      justifyContent: 'flex-end',
      //alignItems: 'flex-start',
      alignItems: 'flex-end',
      flexDirection: 'column',
    },
  },
}));


export const menuEntries = [
  {
    menuTxt: "Suivi de projet / processus",
    txt: (
      <>
        suivi de projet
        <br /> et de processus
      </>
    ),
    href: '/suivi',
    src: ImportedGraphics.icona,
    color: '#1C97C3',
  },
  {
    menuTxt: "Documentation / Archivage",
    // documentation&nbsp;&nbsp;&nbsp; = pour avoir la meme longueur que éditorialisation pour que flex-end align pareil
    txt: (
      <>
        documentation&nbsp;&nbsp;&nbsp;
        <br /> et archivage
      </>
    ),
    href: '/documentation',
    src: ImportedGraphics.iconb,
    color: '#8D3750',
  },
  {
    menuTxt: "Aggrégation de documents",
    txt: (
      <>
        agrégation
        <br /> et exploration
        <br /> de ressources
        <br /> documentaires
      </>
    ),
    href: '/agregation',
    src: ImportedGraphics.iconc,
    color: '#FA8823',
  },
  {
    menuTxt: "Éditorialisation de contenus",
    txt: (
      <>
        éditorialisation
        <br /> de contenus
      </>
    ),
    href: '/editorialisation',
    src: ImportedGraphics.icond,
    color: '#20A7AF',
  },
];

export function IconEntries(props) {
  const classes = useStyles();

  // affiche une des 4 icons avec le texte
  function renderEntrie(entrie) {
    return (
      <UnstyledLink to={entrie.href} className={classes.entrie}>
        <>
          <img src={entrie.src} className={classes.entrieLogo} />
          <div className={classes.entrieText} style={{color: entrie.color}}>
            {entrie.txt}
          </div>
        </>
      </UnstyledLink >
    );
  }

  return (
    <>
      <Grid container direction="row" justify="space-around" alignItems="flex-start">
        <Grid item xs={12} sm={6} className={clsx(classes.entrieContainer, classes.alignTop)}>
          {renderEntrie(menuEntries[0])}
        </Grid>
        <Grid item xs={12} sm={6} className={clsx(classes.entrieContainer, classes.alignBottom)}>
          {renderEntrie(menuEntries[1])}
        </Grid>
      </Grid>
      <Grid container direction="row" justify="space-around" alignItems="flex-start">
        <Grid item xs={12} sm={6} className={clsx(classes.entrieContainer, classes.alignTop)}>
          {renderEntrie(menuEntries[2])}
        </Grid>
        <Grid item xs={12} sm={6} className={clsx(classes.entrieContainer, classes.alignBottom)}>
          {renderEntrie(menuEntries[3])}
        </Grid>
      </Grid>
    </>
  );
}
