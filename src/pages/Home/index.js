import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ImportedGraphics from '../../ImportedGraphics';
import useMediaQueries from '../../utilities/useResponsive';
import YoutubeVideo from '../../components/YoutubeVideo';
import {IconEntries} from './IconEntries';
import TextEntries from './TextEntries';
import globalStyles from '../../globalStyles';
import Config from '../../Config';

const useStyles = makeStyles((theme) => ({
  desktopSpacer: {
    paddingTop: Config.logoTop * 3
  },
  mobileSpacer: {
    paddingTop: Config.logoTop,
  },
  alignCenter: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'column',
  },

  logoContainer: {
    [theme.breakpoints.up(Config.theme.breakpoints)]: {
      paddingLeft: Config.logoTop * 2,
    },
  },
  logo: {
    width: `min(70vw,560px)`,
    height: 'auto',
  },
  subLogoText: {
    fontSize: '4rem',
    fontFamily: 'Lato, sans-serif',
    fontWeight: 'bold',
    paddingTop: '6px',
    paddingLeft: '22px',
    [theme.breakpoints.down('sm')]: {
      fontSize: 'calc(10px + 2vmin)',
      paddingLeft: '12px',
    },
  },
  /*  videoContainer: {
    ... 
    width: '85%',
    maxWidth: '800px',
    height: '0', // crée une hauteur pour la video 
    [theme.breakpoints.down('sm')]: {
      width: '100%', ...
    },   
  },*/
  videoContainer: {
    overflow: 'hidden',
    width: '100%',     
    height: '0', // crée une hauteur pour la video
    paddingBottom: '35%', // crée une hauteur pour la video
    [theme.breakpoints.down('sm')]: {       
      width: '90%',
      paddingBottom: '55%',
    },
    [theme.breakpoints.up('md')]: {       
      height: '215px',
    },
    position: 'relative',
    backgroundColor: Config.colors.blue,
  },
  relativeParent: {
    position: 'relative',
    width: '100%',
  },
  /*  absoluteBackgroundA: {
    position: 'absolute',
    left: '-100%',
    right: '-100%',
   
    height: '100%',
    zIndex: '-1',
    background: `url(${ImportedGraphics.wwwww}) no-repeat `,
    backgroundPosition: '0% 0%',
  },*/
  absoluteBackground: {
    position: 'absolute',
    background: `url(${ImportedGraphics.ww}) repeat-x`,
    backgroundPosition: '50%',
    backgroundSize: '300px', // backgroundSize et height doivent avoir meme valeur
    height: '300px',
    left: '-100%',
    right: '-100%',
    bottom: '-100%',
    [theme.breakpoints.down(Config.theme.breakpoints)]: {
      bottom: '-40%',
    },
    zIndex: '-1',
    transform: 'rotate(-17deg)',
    MozTransform: 'rotate(-17deg)',
    WebkitTransform: 'rotate(-17deg)',
    transformOrigin: 'left center',
    MozTransformOrigin: 'left center',
    WebkitTransformOrigin: 'left center',
  },
}));

export default function Home() {
  const classes = useStyles();
  const gS = globalStyles();
  const { isDesktop } = useMediaQueries();
  useEffect(() => {
    document.title = 'Weever';
  }, []);

  return (
    <div >
      <div className={isDesktop ? classes.desktopSpacer : classes.mobileSpacer} />
      <div className={classes.logoContainer}>
        <img src={ImportedGraphics.weever} className={classes.logo} />
        <div className={classes.subLogoText}>
          du document à la connaissance
          <br />
          partageable et actionnable
        </div>
      </div>
      <div className={classes.alignCenter}>
        <div className={isDesktop ? classes.desktopSpacer : classes.mobileSpacer} />
        <div className={classes.videoContainer}>
          <YoutubeVideo src="https://www.youtube.com/embed/NdyfNP48MCc" />
        </div>
        <div className={classes.relativeParent}>
          <div className={classes.absoluteBackground}></div>
          <div className={isDesktop ? classes.desktopSpacer : classes.mobileSpacer} />
          <IconEntries />
        </div>

        <div className={isDesktop ? classes.desktopSpacer : classes.mobileSpacer} />
        <TextEntries />
      </div>
    </div>
  );
}
