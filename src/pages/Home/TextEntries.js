import React from 'react';
import {Grid} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import ImportedGraphics from '../../ImportedGraphics';
import Config from '../../Config';
import {UnstyledLink} from "../../components/widgets/UnstyledLink";
import globalStyles from '../../globalStyles';

const useStyles = makeStyles((theme) => ({
  alignLeft: {
    [theme.breakpoints.up(Config.theme.breakpoints)]: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'flex-start',
      flexDirection: 'column',
    },
  },
  alignRight: {
    [theme.breakpoints.up(Config.theme.breakpoints)]: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'flex-end',
      flexDirection: 'column',
    },
  },
  container: {
    width: '55%',
    /*[theme.breakpoints.between('md', 'lg')]: {
      width: '60%',
    },*/
    [theme.breakpoints.down(Config.theme.breakpoints)]: {
      width: '100%',
    },
  },
  greenContainer: {
    fontFamily: 'Lato, sans-serif',
    fontWeight: 'Bold',
    fontStyle: 'italic',
    fontSize: '3.2rem',
    padding: '40px 25px',
    [theme.breakpoints.between(Config.theme.breakpoints, 'md')]: {
      fontSize: 'calc(5px + 2vmin)',
    },
    [theme.breakpoints.down(Config.theme.breakpoints)]: {
      fontSize: 'calc(10px + 2vmin)',
      padding: '12px',
    },

    backgroundColor: Config.colors.green,
    marginBottom: '10px',
  },

  blackContainer: {
    border: '3px solid black',
    padding: '15px',
    fontFamily: 'Lato, sans-serif',
    fontWeight: 'Bold',
    fontStyle: 'italic',
    fontSize: '2.3rem',
    [theme.breakpoints.down(Config.theme.breakpoints)]: {
      fontSize: 'calc(8px + 2vmin)',
    },
    marginBottom: '10px',
  },
  arrowLogo: {
    marginRight: '10px',
    width: '25px',
    height: 'auto',
    [theme.breakpoints.down(Config.theme.breakpoints)]: {
      marginRight: '5px',
      width: '15px',
    },
  },
  /*  logo: {
    height: `80%`,
    maxHeight: '75px',
    width: 'auto',
  },*/
  spacearound: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    /* justifyContent: 'space-around',
    [theme.breakpoints.down(Config.theme.breakpoints)]: {
      justifyContent: 'space-between',
    },*/
  },
}));

export default function TextEntries(props) {
  const classes = useStyles();
  const gS = globalStyles();
  
  return (
    <>
      <Grid container direction="row" justify="space-around" alignItems="flex-start">
        <Grid item xs={12} className={clsx(classes.alignLeft)}>
          <div className={classes.container}>

            <UnstyledLink to={"roadmap"}>
              <div className={classes.greenContainer}>
                <div className={classes.spacearound}>
                  <div>
                    La possibilité de développer <br />
                  ensemble le futur de Weever
                </div>
                  <div>
                    <img src={ImportedGraphics.arrowdown} className={classes.arrowLogo} />
                  </div>
                </div>
              </div>
            </UnstyledLink>

            {/* <Grid container direction="row" justify="space-around" alignItems="center" className={classes.greenContainer}>
              <Grid item>
                La possibilité de développer <br />
                ensemble le futur de Weever
              </Grid>
              <Grid item>
                <img src={ImportedGraphics.arrowdown} className={classes.logo} />
              </Grid>
            </Grid>*/}
          </div>
        </Grid>
        <Grid item xs={12} className={clsx(classes.alignRight)}>
          <div className={classes.container}>
            <Grid container direction="column" justify="center" alignItems="stretch">
              <Grid item xs={12} className={classes.blackContainer}>
                Publications automatisées
              </Grid>
              <Grid item xs={12} className={classes.blackContainer}>
                Analyse de vos données, création de liens et <br />
                recommandations intelligentes
              </Grid>
              <Grid item xs={12} className={classes.blackContainer}>
                ...
              </Grid>
            </Grid>
          </div>
        </Grid>
      </Grid>
    </>
  );
}
