import React from 'react';
import PageContainer from '../components/PageContainer';
import globalStyles from '../globalStyles';

export default function MentionsLegales(props) {
  const gS = globalStyles();

  return (
    <PageContainer title="Mentions Légales">
      <div>
        <div className={gS.latoBold15}>
          <p>Éditeur</p>
        </div>
        <div>
          <p>
            Ce site est édité et hébergé par la coopérative <a href="http://mnemotix.com">Mnemotix</a> pour le compte de la communauté des usagers de Weever
           </p>
        </div>
        <div className={gS.latoBold15}>
          <p>Crédits</p>
        </div>
        <div >
          <ul>
            <li>Développement : <a href="http://mnemotix.com">Mnemotix</a> </li>
            <li>Video, Conception graphique et UX : <a href="">Duodaki</a></li>
          </ul>
        </div>
      </div>
    </PageContainer>
  );
}
