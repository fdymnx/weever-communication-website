import Config from './Config';
import {makeStyles} from '@material-ui/core/styles';

const globalStyles = makeStyles((theme) => ({
  tester: {
    border: '1px solid green',
    padding: '1px',
  },
  tester2: {
    border: '2px dotted red',
    padding: '1px',
  },
  tester3: {
    backgroundColor: '#F4A75F',
  },
  flexCenter: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    //flex: 1,
  },
  flexSpaceBetween: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1,
    width: '100%',
  },
  flexStart: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    flex: 1,
  },
  flexEnd: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    flex: 1,
  },
  flexDirectionCol: {
    flexDirection: 'column',
  },
  p20: {
    padding: '20px !important',
  },
  p10: {
    padding: '10px !important',
  },
  ahref: {
    listStyle: 'none',
    lineHeight: '20px',
    fontWeight: '700',
    textDecoration: 'none',
    border: '1px solid rgba(255, 255, 255, 0)',
    borderRadius: '3px',
    zIndex: '1',
    color: Config.colors.dark,
    '&:hover': {
      color: Config.colors.blue,
    },
  },
  /*latoBold: {
    fontFamily: 'LatoBold, sans-serif',    
    fontWeight: 'bold',
    fontSize: '2rem',
  },*/
  latoBold15: {
    fontFamily: 'LatoBold, sans-serif',
    fontWeight: 'bold',
    fontSize: 'calc(15px + 1vmin)',
  },

  latoBlack: {
    fontFamily: 'LatoBlack, sans-serif',
    fontWeight: 'bold'
  },
  width100: {
    width: '100%',
  },
  pointerCursor: {cursor: 'pointer'},
  font14: {
    fontSize: '1.4rem',
  },
  font12: {
    fontSize: '1.2rem',
  },
  maxWidthImg:{
    width : '100%',
    height: "auto"
  },
  selected: {
    color: `${Config.colors.blue} ! important`
  },
}));

export default globalStyles;
