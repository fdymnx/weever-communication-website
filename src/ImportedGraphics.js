// editer le fichier avec adobe illustrator pour changer l'année
import copyright from '../public/images/copyright.svg';
import copyright2021 from '../public/images/copyright2021.svg';
import copyright2022 from '../public/images/copyright2022.svg';
import copyright2023 from '../public/images/copyright2023.svg';
import copyright2024 from '../public/images/copyright2024.svg';
import copyright2025 from '../public/images/copyright2025.svg';
import arrowdown from '../public/images/arrowdown.svg';

import icona from '../public/images/icon_a.svg';
import iconb from '../public/images/icon_b.svg';
import iconc from '../public/images/icon_c.svg';
import icond from '../public/images/icon_d.svg';
import logo from '../public/images/logo.svg';
import weever from '../public/images/weever.svg';
import wwwww from '../public/images/wwwww.svg';
import ww from '../public/images/ww.svg';


import whatSkos from '../public/images/pages/what-skos.gif';
import editDrupal from '../public/images/pages/editorialisation-site-drupal.png';
import lafayetteRebond from '../public/images/pages/lafayette-rebond.jpg';
import weeverEvent from '../public/images/pages/weever-event.png';
import weeverEventGif from '../public/images/pages/weever-event.gif';
import ligneProjet from '../public/images/pages/ligne-projet.png';
import workflowLuma from '../public/images/pages/workflow-luma.png';

const ImportedGraphics = {
  copyright,
  copyright2021,
  copyright2022,
  copyright2023,
  copyright2024,
  copyright2025,
  icona,
  iconb,
  iconc,
  icond,
  logo,
  weever,
  wwwww,
  ww,
  arrowdown, whatSkos, editDrupal, lafayetteRebond, weeverEvent, weeverEventGif, ligneProjet, workflowLuma
};
export default ImportedGraphics;
